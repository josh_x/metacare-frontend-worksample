import { Box, Stack, Tabs } from "@mui/material";
import { useState } from "react";
import AnalyticsGraph from "../components/AnalyticsGraph";
import TabPanel from "../components/TabPanel";
import { TabStyle } from "../styles/main.style";
import {
  AVERAGE_RESOLUTION_TIME,
  AVERAGE_RESPONSE_TIME,
  FIRST_CONTACT_RESOLUTION_RATE,
  REPLIES_PER_RESOLUTION,
} from "../utils/chart";

function a11yProps(index) {
  return {
    id: `mc-analytics-${index}`,
    "aria-controls": `mc-analytics-tabpanel-${index}`,
  };
}

export default function Analytics() {
  const [value, setValue] = useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <>
      <Box sx={{ width: "100%" }}>
        <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="basic tabs example"
          >
            <TabStyle label="Efficiency" {...a11yProps(0)} />
            <TabStyle label="Volume" {...a11yProps(1)} />
            <TabStyle label="Customer Satisfaction" {...a11yProps(2)} />
            <TabStyle label="Backlog" {...a11yProps(2)} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <Stack direction="column" spacing={3}>
            <AnalyticsGraph
              options={AVERAGE_RESPONSE_TIME.options}
              series={AVERAGE_RESPONSE_TIME.series}
              title="Average response Time"
              widget={[
                { title: "Average Response Time", time: "30 Mins" },
                { title: "Response Time", time: "1 Hour 30 Mins" },
              ]}
            />
            <AnalyticsGraph
              options={REPLIES_PER_RESOLUTION.options}
              series={REPLIES_PER_RESOLUTION.series}
              title="Replies per resolution"
              widget={[
                { title: "Average Replies", time: "30 Mins" },
                { title: "Response Time", time: "1 Hour 30 Mins" },
              ]}
            />
            <AnalyticsGraph
              options={AVERAGE_RESOLUTION_TIME.options}
              series={AVERAGE_RESOLUTION_TIME.series}
              title="Average resolution time"
              widget={[
                { title: "Average Resolution Rate", time: "30 Mins" },
                { title: "Response Time", time: "1 Hour 30 Mins" },
              ]}
            />
            <AnalyticsGraph
              options={FIRST_CONTACT_RESOLUTION_RATE.options}
              series={FIRST_CONTACT_RESOLUTION_RATE.series}
              title="First contact resolution rate"
              widget={[
                { title: "Average Contact Rate", time: "30 Mins" },
                { title: "Response Time", time: "1 Hour 30 Mins" },
              ]}
            />
          </Stack>
        </TabPanel>
        <TabPanel value={value} index={1} />
        <TabPanel value={value} index={2} />
      </Box>
    </>
  );
}
