import { Tab } from "@mui/material";
import { styled } from "@mui/material/styles";

export const TabStyle = styled(Tab)(({ theme }) => ({
  textTransform: "capitalize",
  color: theme.palette.primary.main,
  "&.Mui-selected": {
    color: theme.palette.secondary.main,
  },
}));
