import MetacareRouter from "./routes";
import GlobalStyles from "./theme/global-styles";
import ThemeConfig from "./theme/ThemeConfig";

const inputGlobalStyles = <GlobalStyles />;
function App() {
  return (
    <ThemeConfig>
      {inputGlobalStyles}
      <MetacareRouter />
    </ThemeConfig>
  );
}

export default App;
