import pathResolver from "../utils/pathResolver";

const ROOTS = "/";

export const PATH_DASHBOARD = {
  root: ROOTS,
  admin: { root: "/admin" },
  knowledgeBase: { root: "/knowledge-base" },
  trainSAM: { root: "/training" },
  helpCenter: { root: "/help-center" },
  agentInbox: { root: "/agent-inbox" },
  analytics: {
    root: pathResolver(ROOTS, "/analytics"),
    teams: pathResolver(ROOTS, "analytics/teams"),
    knowledgeBase: pathResolver(ROOTS, "analytics/knowledgebase"),
    training: pathResolver(ROOTS, "analytics/training"),
    helpCenter: pathResolver(ROOTS, "analytics/help-center"),
  },
};
