import { useRoutes } from "react-router-dom";
import DashboardWrapper from "../layouts/navbar";
import Analytics from "../pages/Analytics";

export default function MetacareRouter() {
  return useRoutes([
    {
      path: "/",
      element: <DashboardWrapper />,
      children: [{ path: "", element: <Analytics /> }],
    },
    {
      path: "/analytics",
      element: <DashboardWrapper />,
      children: [
        { path: "teams", element: <Analytics /> },
        { path: "knowledgebase", element: <Analytics /> },
        { path: "training", element: <Analytics /> },
        { path: "help-center", element: <Analytics /> },
      ],
    },
  ]);
}
