import SvgIconStyle from "../components/SvgIconStyle";
import { PATH_DASHBOARD } from "../routes/path";

const getIcon = (name) => (
  <SvgIconStyle
    src={`../assets/icons/${name}.svg`}
    sx={{ width: "100%", height: "100%" }}
  />
);

const ICONS = {
  users: getIcon("mc_user"),
};

// URL for the sidebar menus
const sidebarConfig = [
  {
    items: [
      {
        title: "admin",
        path: PATH_DASHBOARD.admin.root,
        icon: ICONS.users,
        children: [],
      },
      {
        title: "knowledge base",
        path: PATH_DASHBOARD.knowledgeBase.root,
        icon: ICONS.users,
        children: [],
      },
      {
        title: "train SAM",
        path: PATH_DASHBOARD.trainSAM.root,
        icon: ICONS.users,
        children: [],
      },
      {
        title: "help center",
        path: PATH_DASHBOARD.helpCenter.root,
        icon: ICONS.users,
        children: [],
      },
      {
        title: "agent inbox",
        path: PATH_DASHBOARD.agentInbox.root,
        icon: ICONS.users,
        children: [],
      },
      {
        title: "analytics",
        path: PATH_DASHBOARD.analytics.root,
        children: [
          {
            title: "teams",
            path: PATH_DASHBOARD.analytics.teams,
            icon: "",
          },
          {
            title: "knowledge base",
            path: PATH_DASHBOARD.analytics.knowledgeBase,
            icon: "",
          },
          {
            title: "training",
            path: PATH_DASHBOARD.analytics.training,
            icon: "",
          },
          {
            title: "help center",
            path: PATH_DASHBOARD.analytics.helpCenter,
            icon: "",
          },
        ],
        icon: ICONS.users,
      },
    ],
  },
];

export default sidebarConfig;
