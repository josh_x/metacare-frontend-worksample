import arrowIosDownwardFill from "@iconify/icons-eva/arrow-ios-downward-fill";
import arrowIosForwardFill from "@iconify/icons-eva/arrow-ios-forward-fill";
import { Icon } from "@iconify/react";
import { Box, Collapse, List, ListItemText } from "@mui/material";
import PropTypes from "prop-types";
import { useState } from "react";
import {
  matchPath,
  NavLink as RouterLink,
  useLocation,
} from "react-router-dom";
import { v4 as uuidv4 } from "uuid";
import { ListItemIconStyle, ListItemStyle } from "../../styles/navbar.style";

// ----------------------------------------------------------------------

NavItem.propTypes = {
  isShow: PropTypes.bool,
  item: PropTypes.object,
};
function NavItem({ item, isShow }) {
  const { pathname } = useLocation();
  const { title, path, icon, info, children } = item;
  const isActiveRoot = path
    ? !!matchPath({ path, end: false }, pathname)
    : false;

  const [open, setOpen] = useState(isActiveRoot);

  const handleOpen = () => {
    setOpen(!open);
  };

  // style parent menu
  const activeRootStyle = {
    color: "info.main",
    fontWeight: "800",
    "&:before": { display: "block" },
  };

  // style child menu
  const activeSubStyle = {
    color: "text.secondary",
  };

  // corresponding style if parent has sub menus
  if (children) {
    return (
      <>
        <ListItemStyle
          onClick={handleOpen}
          sx={[
            {
              ...(isActiveRoot && activeRootStyle),
            },
          ]}
        >
          <ListItemIconStyle>{icon && icon}</ListItemIconStyle>

          {isShow && (
            <>
              <ListItemText disableTypography primary={title} />
              {info && info}
              <Box
                component={Icon}
                icon={open ? arrowIosDownwardFill : arrowIosForwardFill}
                sx={{ width: 16, height: 16, ml: 1 }}
              />
            </>
          )}
        </ListItemStyle>

        {isShow && (
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {children.map((item) => {
                const { title, path } = item;
                const isActiveSub = path
                  ? !!matchPath({ path, end: false }, pathname)
                  : false;
                return (
                  <ListItemStyle
                    key={title}
                    component={RouterLink}
                    to={path}
                    sx={{
                      ...(isActiveSub && activeSubStyle),
                      paddingLeft: "50px",
                    }}
                  >
                    <ListItemText disableTypography primary={title} />
                  </ListItemStyle>
                );
              })}
            </List>
          </Collapse>
        )}
      </>
    );
  }

  return (
    <ListItemStyle
      component={RouterLink}
      to={path}
      sx={{
        ...(isActiveRoot && activeRootStyle),
      }}
    >
      <ListItemIconStyle>{icon && icon}</ListItemIconStyle>
      {isShow && (
        <>
          <ListItemText disableTypography primary={title} />
          {info && info}
        </>
      )}
    </ListItemStyle>
  );
}

TheMenuSection.propTypes = {
  isShow: PropTypes.bool,
  navConfig: PropTypes.array,
};
export default function TheMenuSection({ navConfig, isShow = true, ...other }) {
  return (
    <Box {...other}>
      {navConfig.map((list) => {
        const { items } = list;
        return (
          <List key={uuidv4()} disablePadding>
            {items.map((item) => (
              <NavItem key={uuidv4()} item={item} isShow={isShow} />
            ))}
          </List>
        );
      })}
    </Box>
  );
}
