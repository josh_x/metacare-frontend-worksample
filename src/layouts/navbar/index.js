import * as React from "react";
import Box from "@mui/material/Box";
import Drawer from "@mui/material/Drawer";
import CssBaseline from "@mui/material/CssBaseline";
import Toolbar from "@mui/material/Toolbar";
import { MCAppBar } from "../../styles/navbar.style";
import sidebarConfig from "../sidebar-config";
import UserInfoCard from "../../components/UserInfoCard";

import {
  SearchBar,
  SearchBarIconWrapper,
  StyledInputBase,
} from "../../styles/searchbar.style";
import { IconButton } from "@mui/material";
import TheMenuSection from "./TheMenuSection";
import { DRAWER_WIDTH } from "../../utils/constants";
import { Outlet } from "react-router-dom";

export default function DashboardWrapper() {
  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <MCAppBar position="fixed" open={open}>
        <Toolbar>
          <SearchBar>
            <SearchBarIconWrapper>{/* <SearchIcon /> */}</SearchBarIconWrapper>
            <StyledInputBase
              placeholder="Ask us any question"
              inputProps={{ "aria-label": "search" }}
            />
          </SearchBar>
          <Box>
            <IconButton
              size="large"
              aria-label="show 4 new mails"
              color="inherit"
            ></IconButton>
          </Box>
        </Toolbar>
      </MCAppBar>
      <Drawer
        sx={{
          width: DRAWER_WIDTH,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: DRAWER_WIDTH,
            boxSizing: "border-box",
            px: "10px",
            borderColor: (theme) => theme.palette.widget.border[0],
          },
        }}
        variant="permanent"
        anchor="left"
      >
        <UserInfoCard title="Metacare" email="adeyinka@metacare.app" />

        <TheMenuSection navConfig={sidebarConfig} />
      </Drawer>
      <Box
        component="main"
        sx={{ flexGrow: 1, bgcolor: "background.default", p: 3 }}
      >
        <Toolbar />
        <Outlet />
      </Box>
    </Box>
  );
}
