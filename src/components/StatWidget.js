import PropTypes from "prop-types";
import { Box, Typography } from "@mui/material";

StatWidget.propTypes = {
  title: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
};

export default function StatWidget({ title, time }) {
  return (
    <Box
      sx={(theme) => ({
        border: `1px solid ${theme.palette.widget.border[0]}`,
        borderRadius: 2.5,
        p: 3,
        backgroundColor: theme.palette.widget.background[0],
      })}
    >
      <Typography
        sx={{
          color: (theme) => theme.palette.primary.main,
          fontSize: "0.875rem",
        }}
      >
        {title}
      </Typography>
      <Typography variant="h3">{time}</Typography>
    </Box>
  );
}
