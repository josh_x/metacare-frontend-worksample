import { Box, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";

UserInfoCard.propTypes = {
  title: PropTypes.string,
  email: PropTypes.string,
};

export default function UserInfoCard({ title, email }) {
  const theme = useTheme();

  const RootStyle = styled("div")(() => ({
    display: "flex",
    flexDirection: "column",
    border: `1px solid ${theme.palette.widget.border[0]}`,
    borderRadius: "8px",
    alignItems: "center",
    padding: "8px",
    margin: "8px",
  }));

  return (
    <RootStyle>
      <Box>
        <Typography variant="subtitle1">{title}</Typography>
        <Typography
          variant="caption"
          sx={{
            fontFamily: "'Gelion Light', Arial",
          }}
        >
          {email}
        </Typography>
      </Box>
    </RootStyle>
  );
}
