import { Card, CardHeader, Grid, Stack } from "@mui/material";
import React from "react";
import ReactApexChart from "react-apexcharts";
import StatWidget from "./StatWidget";
import PropTypes from "prop-types";
import { v4 as uuidv4 } from "uuid";

AnalyticsGraph.propTypes = {
  title: PropTypes.string.isRequired,
  options: PropTypes.object.isRequired,
  series: PropTypes.object.isRequired,
  widget: PropTypes.array.isRequired,
};
export default function AnalyticsGraph({ title, options, series, widget }) {
  return (
    <Stack direction="row" spacing={3}>
      <Card sx={{ width: "100%" }} variant="outlined">
        <Grid container columnSpacing={3} spacing={3}>
          <Grid item xs={8}>
            <Card
              variant="outlined"
              sx={{ borderTop: 0, borderBottom: 0, borderLeft: 0 }}
            >
              <CardHeader title={title} />

              <ReactApexChart options={options} series={series} type="line" />
            </Card>
          </Grid>
          <Grid item xs={4} sx={{ margin: "auto" }}>
            <Stack direction="column" spacing={3} sx={{ paddingRight: 3 }}>
              {widget.map((widget) => (
                <StatWidget
                  key={uuidv4()}
                  title={widget.title}
                  time={widget.time}
                />
              ))}
            </Stack>
          </Grid>
        </Grid>
      </Card>
    </Stack>
  );
}
