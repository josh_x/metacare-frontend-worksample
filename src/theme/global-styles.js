import { GlobalStyles as GlobalThemeStyles } from "@mui/material";
import { useTheme } from "@mui/material/styles";

export default function GlobalStyles() {
  const theme = useTheme();

  return (
    <GlobalThemeStyles
      styles={{
        "*": {
          margin: 0,
          padding: 0,
          boxSizing: "border-box",
        },
        "span.MuiCardHeader-title": {
          color: theme.palette.secondary.main,
          fontFamily: "Gelion Semi Bold",
          fontSize: "1.125rem",
        },
        ".MuiListItemButton-root": {
          fontSize: 16,
          color: theme.palette.primary.main,
        },
        ".MuiTypography-subtitle1": {
          color: theme.palette.secondary.main,
        },
        ".MuiTypography-caption": {
          color: theme.palette.primary.main,
        },
        ".MuiTabs-flexContainer + .MuiTabs-indicator": {
          background: theme.palette.info.main,
        },
        ".MuiTypography-h3": {
          color: theme.palette.secondary.main,
        },
      }}
    />
  );
}
