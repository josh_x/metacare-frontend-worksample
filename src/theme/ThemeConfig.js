import PropTypes from "prop-types";
import GelionRegular from "../assets/fonts/gelion/Gelion-Regular.woff";
import GelionSemiBold from "../assets//fonts/gelion/Gelion-SemiBold.woff";
import GelionBold from "../assets//fonts/gelion/Gelion-Bold.woff";
import {
  createTheme,
  ThemeProvider,
  StyledEngineProvider,
} from "@mui/material/styles";
import { CssBaseline } from "@mui/material";
import typography from "./typgography";
import palette from "./palette";

ThemeConfig.propTypes = {
  children: PropTypes.node,
};

export default function ThemeConfig({ children }) {
  const theme = createTheme({
    typography,
    palette: palette.light,
    components: {
      MuiCssBaseline: {
        styleOverrides: `
              @font-face {
                font-family: 'Gelion Regular';
                font-style: normal;
                font-display: swap;
                font-weight: normal;
                src: url(${GelionRegular}) format("woff");
              }
              
              @font-face {
                font-family: 'Gelion Bold';
                font-style: normal;
                font-display: swap;
                font-weight: normal;
                src: url(${GelionBold}) format("woff");
              }

              @font-face {
                font-family: 'Gelion Semi Bold';
                font-style: normal;
                font-display: swap;
                font-weight: normal;
                src: url(${GelionSemiBold}) format("woff");
              }
            `,
      },
    },
  });

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        {children}
      </ThemeProvider>
    </StyledEngineProvider>
  );
}
