const fontWeightRegular = 400;
const fontWeightMedium = 600;
const fontWeightBold = 700;

function pxToRem(value) {
  return `${value / 16}rem`;
}

function responsiveFontSizes({ sm, md = "inherit", lg = "inherit" }) {
  return {
    "@media (min-width:600px)": {
      fontSize: pxToRem(sm),
    },
    "@media (min-width:900px)": {
      fontSize: pxToRem(md),
    },
    "@media (min-width:1200px)": {
      fontSize: pxToRem(lg),
    },
  };
}

const FONT_PRIMARY = "'Gelion Regular', Arial";

const typography = {
  fontFamily: FONT_PRIMARY,
  fontWeightRegular,
  fontWeightMedium,
  fontWeightBold,
  h3: {
    lineHeight: "160%",
    fontWeight: fontWeightMedium,
    fontSize: pxToRem(20),
    ...responsiveFontSizes({ sm: 20 }),
  },
  subtitle1: {
    lineHeight: "22px",
    fontSize: pxToRem(14),
    ...responsiveFontSizes({ sm: 14 }),
  },
  caption: {
    lineHeight: "20px",
    fontSize: pxToRem(2),
    ...responsiveFontSizes({ sm: 12 }),
  },
};

export default typography;
