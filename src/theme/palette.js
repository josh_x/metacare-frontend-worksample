import { alpha } from "@mui/material/styles";

const PRIMARY = {
  main: "#696D8C",
  contrastText: "#FFFFFF",
};
const SECONDARY = {
  main: "#060213;",
  contrastText: "#FFFFFF",
};

const INFO = {
  main: "#6837EF",
  contrastText: "#fff",
};

const SUCCESS = {
  main: "#25BB87",
  contrastText: "#fff",
};

const CHART_COLORS = {
  badge: ["#25BB87", "#25BB871A"],
  lines: { red: ["#F05D23", "#FB6491"], blue: ["#3E68FF", "#07C9E2"] },
};

const WIDGETS = {
  border: ["#ECEBF5", "#25BB871A"],
  background: ["#FAFAFA"],
  lines: { red: ["#F05D23", "#FB6491"], blue: ["#3E68FF", "#07C9E2"] },
};

const COMMON = {
  common: { black: "#000", white: "#fff" },
  primary: { ...PRIMARY },
  secondary: { ...SECONDARY },
  info: { ...INFO },
  success: { ...SUCCESS },
  chart: CHART_COLORS,
  widget: WIDGETS,
  action: {
    hover: alpha(INFO.main, 0.08),
    active: INFO.main,
    disabledBackground: "#FAFAFC",
  },
};

const palette = {
  light: {
    ...COMMON,
    text: { primary: PRIMARY.main, secondary: SECONDARY.main },
    background: { paper: "#FFFFFF", default: "#FFFFFF" },
  },
};

export default palette;
